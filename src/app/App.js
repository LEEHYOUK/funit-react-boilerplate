import React from 'react';
import Redux from './Redux';
import Router from './Router';

class App extends React.Component {
    render () {
        return (
            <Redux>
                <Router/>
            </Redux>
        );
    }
}

export default App;
