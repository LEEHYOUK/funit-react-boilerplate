import React from 'react';
import {Provider} from 'react-redux';
import {createStore, combineReducers, applyMiddleware} from 'redux';
import {ConnectedRouter, routerReducer, routerMiddleware, push} from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';
import thunk from 'redux-thunk';

import * as reducer from '../redux/reducer';

const history = createHistory();
const middleware = routerMiddleware(history);
const store = createStore(combineReducers({
	...reducer,
	router: routerReducer
}), applyMiddleware(thunk, middleware));

const Redux = (props) => {
	return (
		<Provider store={store}>
			<ConnectedRouter history={history}>
				{props.children}
			</ConnectedRouter>
		</Provider>
	)
}

export default Redux
