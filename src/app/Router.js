import React from 'react';
import {Switch, Route} from 'react-router';
import Page1 from '../component/Page1';
import Page2 from '../component/Page2';

const Router = (props) => {
	return (
		<Switch>
			<Route path="/next" component={Page2}/>
			<Route path="/" component={Page1}/>
		</Switch>
	)
}

export default Router;
