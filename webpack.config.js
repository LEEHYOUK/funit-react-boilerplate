var webpack = require('webpack');

module.exports = {
	entry: './src/index.js',
	mode: "none",
	output: {
		path: __dirname + '/public',
		filename: 'bundle.js'
	},

	devServer: {
		historyApiFallback: true,
		inline: true,
		port: 80,
		contentBase: __dirname + '/public'
	},

	module: {
		rules: [
			{
				test: /\.js$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
				query: {
					presets: ['env', 'stage-0', 'react']
				}
			}, {
				test: /\.css$/,
				use: [
					{
						loader: 'style-loader'
					}, {
						loader: 'css-loader'
					}
				]
			}, {
				test: /\.scss$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'style-loader'
					}, {
						loader: 'css-loader'
					}, {
						loader: 'sass-loader'
					}
				]
			}
		]
	}
};
